#!/bin/zsh

# dump this code into your zshrc at the top
#
# This terrible zsh profiler requires you
# to use zsh and sed. The sed version
# tested was the one included in mac os x
# mojave.


# Set this variable to what you're going to be profiling.
#
# We read the line straight from the file to see what is executing
SCRIPTPATH=terrible-zsh-profiler.sh

typeset -F SECONDS

profile_report=""

function read_line {
  LINE_NUM=$1
  echo -n "${SCRIPTPATH}:${LINE_NUM} command: $(sed "${LINE_NUM}q;d" $SCRIPTPATH)"
}

last_seconds=$SECONDS

function profile {
  LINE_NUM=$1
  LINE_NUM=$(( $LINE_NUM - 1 ))
  let "elapsed_seconds = ${SECONDS} - $last_seconds"
  last_seconds=${SECONDS}
  profile_report="${profile_report}${elapsed_seconds} "$(read_line $LINE_NUM)"\n"
}

# actual usage:

sleep 3 && echo "sleeping for 3 seconds and then echoing this echo statement"
profile $LINENO

echo -e "\ndone profiling"
echo -e $profile_report
echo -e "total time executing: ${SECONDS}"
