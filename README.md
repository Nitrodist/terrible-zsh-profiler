# Terrible ZSH Profiler

Have you ever wanted to profile your `.zshrc` to figure out what's slow when you launch a prompt? 

Or do you find yourself trying to answer with your Google-fu the question of "can you profile zsh startup"? Look no further! This project which is simple enough for you to throw in anywhere and get useful output.

Introducing the [Terrible ZSH Profiler](https://gitlab.com/Nitrodist/terrible-zsh-profiler). It's dead simple, you could have written it in an afternoon (I know I did).

This script is two functions and a smattering of variables. In exchange, you can drop the code in anywhere and begin profiling immediately. You can also edit the code to what you want, it's very simple. Want to add red color when it exceeds X seconds? Go for it!

Example usage in your script:

```sh

# copy library code from `terrible-zsh-profiler.sh` to the top of
# the zsh script you want to profile

sleep 3 && echo "sleeping for 3 seconds and then echoing this echo statement"
profile $LINENO

echo -e "\ndone profiling"
echo -e $profile_report
echo -e "total time executing: ${SECONDS}"
```

Example output:

```
$ ./terrible-zsh-profiler.sh
sleeping for 3 seconds and then echoing this echo statement

done profiling
3.0075800000 terrible-zsh-profiler.sh:37 command: sleep 3 && echo "sleeping for 3 seconds and then echoing this echo statement"

total time executing: 3.0208390000
```

## Minimal requirements

1. Use zsh and depend on `typeset -F SECONDS` being set to allow for sub-second timing information
2. `sed` (tested on Mac OS X Mojave) is used with the command `sed "${line_number}q;d"`

You can edit the script to not use sed or to port it to gnu sed if it's incompatible.

## Caveats

It only profiles since the last time you called `profile` or from where you set `last_seconds=$SECONDS`.

## More example output of my .zshrc

```
done profiling
0.0075820000 executing: stty -ixon
0.1850910000 executing: source $ZSH/oh-my-zsh.sh
0.0111680000 executing:   source "$HOME/.aliases"
0.0092900000 executing:   source "$HOME/.other_aliases"
0.0067990000 executing:   source "$HOME/.env_vars"
0.1336680000 executing:   eval "$(rbenv init -)"
0.0056610000 executing: [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
0.0386160000 executing:   export PATH="$(pyenv root)/shims:$PATH"
0.0076950000 executing: . "/Users/mark.campbell/.acme.sh/acme.sh.env"
0.0274990000 executing: source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
1.4064680000 executing: [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
0.0756120000 executing: [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

total time executing: 1.9288400000
```
